﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeatSource))]
[RequireComponent(typeof(SpriteRenderer))]
public class Burnable : MonoBehaviour
{
	public float HeatSourceTickTime = 5f;
	public float BurnTime = 15f;
	public ParticleSystem FireParticleSystem;
	public Vector3 FireSpawnPoint = Vector3.zero;

	private ParticleSystem _fire;
	private bool _isBurning = false;
	private HeatSource _heatSource;

	private void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere(transform.position + FireSpawnPoint, 0.21f);
	}

	// Use this for initialization
	void Start ()
	{
		_heatSource = GetComponent<HeatSource>();
		StartCoroutine(CheckForHeatSources());
	}

	private IEnumerator CheckForHeatSources()
	{
		while (true)
		{
            var colliders = Physics.OverlapSphere(transform.position, 1);
			for (var i = 0; i < colliders.Length; i++)
			{
				var heatSource = colliders[i].gameObject.GetComponent<HeatSource>();
				if (heatSource == null || heatSource.HeatLevel.Equals(0)) continue;
				var chance = heatSource.HeatLevel * Random.Range(0f, 1f);
				if (!(chance > 50f)) continue;
				StartBurn();
				yield break;
			}
			yield return new WaitForSeconds(HeatSourceTickTime);
		}
	}

	private void StartBurn()
	{
		_isBurning = true;
		_fire = Instantiate(FireParticleSystem);
		_fire.transform.SetParent(transform, false);
		_fire.transform.localPosition = FireSpawnPoint;
		_fire.transform.localScale = Vector3.one;

		_heatSource.HeatLevel = 20;
		_heatSource.IsBurning = true;
		
		StartCoroutine(BurnCountdown());
	}

	private IEnumerator BurnCountdown()
	{
		yield return new WaitForSeconds(BurnTime);
		EndBurn();
	}

	private void EndBurn()
	{
		if (!_isBurning) return;
		var spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.color = Color.black;
		_fire.Stop();
		_heatSource.IsBurning = false;
		_isBurning = false;
	}
}
