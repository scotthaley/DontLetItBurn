﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
	public float PanSpeed = 0.5f;
	private Vector3 _lastMousePos;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update () {
		
		// Panning
		if (Input.GetMouseButton(0))
		{
			var delta = (_lastMousePos - Input.mousePosition) * PanSpeed;
			transform.position += new Vector3(delta.x, 0, delta.y);
		}
		_lastMousePos = Input.mousePosition;
		
		// Zooming
		var zoomDelta = Input.GetAxis("Mouse ScrollWheel");
		transform.position += transform.forward * zoomDelta;
	}
}
