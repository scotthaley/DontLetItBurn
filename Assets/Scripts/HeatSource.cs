﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class HeatSource : MonoBehaviour
{
	public float HeatLevel = 0;
	public bool IsBurning = false;
	
	// Update is called once per frame
	void Update ()
	{
		if (IsBurning)
			HeatLevel += 3f * Time.deltaTime;
		else if (HeatLevel > 0)
			HeatLevel -= 2 * Time.deltaTime;
		else
			HeatLevel = 0;
	}
}
